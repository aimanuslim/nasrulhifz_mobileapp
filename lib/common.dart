import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

TextStyle nhPlaceholderStyle() {
  return TextStyle(color: Colors.green);
}

TextStyle nhNormalTextStyle() {
  return TextStyle(color: Colors.lightGreen);
}

Padding nhSurahNameLabel(AsyncSnapshot snapshot) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: Container(
              padding: EdgeInsets.all(3),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.green),
                  // color: Colors.green,
                  borderRadius: BorderRadius.circular(5)),
              child: Center(
                child: Text(snapshot.data.name_string,
                    style: TextStyle(fontFamily: 'me', fontSize: 20)),
              )),
        ),
      ],
    ),
  );
}

Widget nhGoButton(displaytext, on_press) {
  return new RaisedButton(
    onPressed: on_press,
    color: Colors.green,
    textColor: Colors.black87,
    child: Text(displaytext),
    splashColor: Colors.lightGreen,
    padding: EdgeInsets.all(15),
    shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
  );
}

Widget formField(placeholder, validator, type) {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: TextFormField(
      keyboardType: type,
      validator: validator,
      inputFormatters: <TextInputFormatter>[
        WhitelistingTextInputFormatter.digitsOnly,
      ],
      autovalidate: true,
      decoration: InputDecoration(
          labelText: placeholder,
          labelStyle: nhPlaceholderStyle(),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5)))),
    ),
  );
}

Widget QuranWord(string) {
  return Text(
    string,
    style: TextStyle(fontFamily: 'me', fontSize: 20),
    textAlign: TextAlign.right,
    textDirection: TextDirection.rtl,
  );
}
