import 'dart:convert';

class Hifz {
  int surah_number;
  int ayat_number;
  DateTime last_refreshed;
  int juz_number;
  int average_difficulty;

  Hifz({this.surah_number, this.ayat_number, this.last_refreshed, this.juz_number, this.average_difficulty});
  factory Hifz.fromJson(Map<String, dynamic> json) {
    return Hifz(
      surah_number: json['surah_number'] as int,
      ayat_number: json['ayat_number'] as int,
      last_refreshed: DateTime.parse(json['last_refreshed']) ,
      juz_number: json['juz_number'] as int,
      average_difficulty: json['average_difficulty'] as int,
    );
  }
}

class QuranMeta {
  int surah_number;
  int ayat_number;
  String ayat_string;
  int juz_number;

  QuranMeta({this.surah_number, this.ayat_number, this.ayat_string, this.juz_number});
  
  factory QuranMeta.fromJson(Map<String, dynamic> json) {
    return QuranMeta(
      surah_number: json['surah_number'] as int,
      ayat_number: json['ayat_number'] as int,
      ayat_string: json['ayat_string'] as String,
      juz_number: json['juz_number'] as int,
    );
  }

  Map<String, dynamic> toJson() =>
    {
      'surah_number': surah_number,
      'ayat_number': ayat_number,
      'ayat_string': ayat_string,
      'juz_number': juz_number,
    };

}

class SurahMeta {
  int surah_number;
  String name_string;
  int surah_ayat_max;

  SurahMeta(this.surah_number, this.name_string, this.surah_ayat_max);

  SurahMeta.fromJson(Map<String, dynamic> json)
      : surah_number = json['surah_number'],
        name_string = json['name_string'],
        surah_ayat_max = json['surah_ayat_max'];

}