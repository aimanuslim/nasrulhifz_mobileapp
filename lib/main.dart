import 'package:flutter/material.dart';
import 'package:nasrulhifz_mobileapp/enter.dart';
import 'list.dart';
import 'revise.dart';

void main() => runApp(NasrulHifzApp());

class NasrulHifzApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'NasrulHifz',
      theme: ThemeData(
        primarySwatch: Colors.green,
        fontFamily: 'OpenSans',
    
        textTheme: TextTheme(
          headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          body1: TextStyle(fontSize: 14.0, fontFamily: 'Consolas'),
        ),
      ),
      home: TabbedHomePage()
    );
  }
}

class TabbedHomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(text: 'List'),
              Tab(text: 'Enter'),
              Tab(text: 'Revise'),
            ]
          ),
          title: Text('NasrulHifz'),
        ),
        body: TabBarView(
          children: <Widget>[
            SurahListPage(),
            EnterAyatPage(),
            RevisePage()
          ],
        )
      )
    );
  }
}
