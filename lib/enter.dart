import 'package:flutter/material.dart';
import "common.dart";

class EnterAyatPage extends StatefulWidget {
  @override
  _EnterAyatPageState createState() => _EnterAyatPageState();
}

class _EnterAyatPageState extends State<EnterAyatPage> {
  String dropdownValue;

  @override
  void initState() {
    dropdownValue = 'Easy';
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          formField("Surah Number", (value) {
            if (value?.isEmpty ?? true) return null;
            var numValue = int.tryParse(value);

            if (numValue > 114) {
              return "Surah's in the Quran is never more than 114";
            }

            return null;
          }, TextInputType.number),
          formField("Ayat Number", null, TextInputType.number),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                border: Border.all(width: 1, color: Colors.grey),
                borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
              
              child: Row(
                children: <Widget>[
                  Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 5),
                                        child: DropdownButtonHideUnderline(
                                                                                child: DropdownButton<String>(
                      value: this.dropdownValue,
                      onChanged: (String newValue) {
                        setState(() {
                          this.dropdownValue = newValue;
                        });
                      },
                      items: <String>['Easy', 'Medium', 'Hard']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: nhNormalTextStyle()),
                        );
                      }).toList(),
                    ),
                                        ),
                                      ),
                  ),
                ],
              ),
            ),
          ),
          nhGoButton("Enter Ayat", () {})
        ],
      ),
    );
  }
}
