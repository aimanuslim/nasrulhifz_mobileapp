import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'models.dart';
import 'requests.dart';

class AyatListPage extends StatefulWidget {
  final List<Hifz> hifzlist;

  AyatListPage({Key key, @required this.hifzlist}) : super(key: key);


  @override
  AyatListState createState(){
    return AyatListState();
  }
}

class AyatListState extends State<AyatListPage> {
    @override
    // this is to inherit the data from the page.
    AyatListPage get widget => super.widget;

    var isLoading = false;


    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text("Hifz List"),
        ),
        body: ListView.builder(
          itemCount: widget.hifzlist.length > 0 ? widget.hifzlist.length + 1 : 1, // the hifzlist from the page class is being used here.
          itemBuilder: (context, index) {
            double c_width = MediaQuery.of(context).size.width*0.8;
            var formatter = new DateFormat('d MMMM yyyy');

            if (index == 0) {
              // return the header
            return Container(
              padding: EdgeInsets.all(10),  
              width: c_width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Text("Surah Number", textAlign: TextAlign.center),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text("Ayat Number", textAlign: TextAlign.center),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text("Juz Number", textAlign: TextAlign.center),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text("Date Last Refreshed", textAlign: TextAlign.center),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text("Average Difficulty", textAlign: TextAlign.center),
                  ),
                ],)
              );
            }

            index -= 1;
            return GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                Navigator.push(context,  MaterialPageRoute(
                builder: (context) => AyatDetailsPage(
                  qm: fetchQuranMeta(widget.hifzlist[index].surah_number, widget.hifzlist[index].ayat_number),
                  sm: fetchSurahMeta(widget.hifzlist[index].surah_number),
                  ayat_number: widget.hifzlist[index].ayat_number,
                    )
                  )
                );
              },
              child: Container(
              padding: EdgeInsets.only(top: 20, bottom: 20,left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Text(widget.hifzlist[index].surah_number.toString(), textAlign: TextAlign.center),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(widget.hifzlist[index].ayat_number.toString(), textAlign: TextAlign.center),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(widget.hifzlist[index].juz_number.toString(), textAlign: TextAlign.center),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(formatter.format(widget.hifzlist[index].last_refreshed), textAlign: TextAlign.center),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(widget.hifzlist[index].average_difficulty.toString(), textAlign: TextAlign.center)
                  ),
                ],
              ),
            )
            );
          }
            
          ),
        );
    }
}

class AyatDetailsPage extends StatelessWidget {
  final Future<QuranMeta> qm;
  final Future<SurahMeta> sm;
  final int ayat_number;

  AyatDetailsPage({Key key, @required this.qm, @required this.sm, @required this.ayat_number}): super(key : key);

  @override 
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: FutureBuilder<SurahMeta>(
            future: sm,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
              return Text.rich(TextSpan(
                children: <TextSpan>[
                  TextSpan(text: "Surah "),
                  TextSpan(text: " ${snapshot.data.name_string.split(" ")[1]} ", style: TextStyle(fontFamily: 'me', fontSize: 25)),
                  TextSpan(text: " Ayat ${this.ayat_number} "),
                ]
                )
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            // By default, show a loading spinner
            return Center(child: CircularProgressIndicator());  
            },
          )
          
        ),
        body:  FutureBuilder<QuranMeta>(
          future: qm,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Center(child: Padding(
                padding: EdgeInsets.all(35),
                child: Text(snapshot.data.ayat_string, textAlign: TextAlign.center, style: TextStyle(fontSize: 30, fontFamily: 'me'),
                )
                )
                );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner
            return Center(child: CircularProgressIndicator());
          },
        )
      );
  }
}

