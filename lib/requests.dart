import 'models.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

String host = "http://aimanuslim.pythonanywhere.com";

    Future<QuranMeta> fetchQuranMeta(surah_number, ayat_number) async {
      final response =
      await http.get('${host}/nasrulhifz/api/quranmeta/${surah_number}/${ayat_number}/');
      // await http.get(new Uri.http("locahost:8000", '/nasrulhifz/api/quranmeta/${surah_number}/${ayat_number}/'));

      if (response.statusCode == 200) {
        // If server returns an OK response, parse the JSON
        return QuranMeta.fromJson(json.decode(utf8.decode(response.bodyBytes)));
      } else {
        // If that response was not OK, throw an error.
        throw Exception('Failed to load post');
      }
    }

    Future<SurahMeta> fetchSurahMeta(surah_number) async {
      final response = 
      await http.get('${host}/nasrulhifz/api/surahmeta/${surah_number}/');


      if (response.statusCode == 200) {
        // If server returns an OK response, parse the JSON
        return SurahMeta.fromJson(json.decode(utf8.decode(response.bodyBytes)));
      } else {
        // If that response was not OK, throw an error.
        throw Exception('Failed to load post');
      }
    }

    Future<List<Hifz>> fetchHifz() async {
      final response = await http.get('${host}/nasrulhifz/api/', headers: {HttpHeaders.authorizationHeader: "Token 74cdf0ede139bc760b3eac6899cb68af57111e2c"},);
      if (response.statusCode == 200) {
        // If server returns an OK response, parse the JSON
        Iterable l = json.decode(utf8.decode(response.bodyBytes));
        List<Hifz> posts = l.map((model)=> Hifz.fromJson(model)).toList();  
        return posts;
      } else {
        // If that response was not OK, throw an error.
        throw Exception('Failed to load post');
      }
    }

