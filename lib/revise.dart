import 'package:flutter/material.dart';
import "common.dart";
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';
import 'package:flutter/gestures.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ReviseWord extends StatefulWidget {
  final String word;

  ReviseWord({Key key, @required this.word}) : super(key: key);

  @override
  _ReviseWordState createState() => _ReviseWordState();
}

class _ReviseWordState extends State<ReviseWord> {
  bool hidden = true;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: QuranWord(
        hidden ? "  ___  " : widget.word,
      ),
      onTap: () {
        setState(() {
          hidden = !hidden;
        });
      },
    );
  }
}

class RevisePage extends StatefulWidget {
  @override
  _RevisePageState createState() => _RevisePageState();
}

class _RevisePageState extends State<RevisePage> {
  bool isFetching = false;
  bool dataReady = false;
  List list = List();

  _fetchData() async {
    setState(() {
      isFetching = true;
      dataReady = true;
    });

    var queryParams = {
      'vicinity': '2',
      'streak_length': '4',
      'blind_count': '1'
    };

    var uri = Uri.https('aimanuslim.pythonanywhere.com',
        '/nasrulhifz/api/revise/', queryParams);

    final response = await http.get(
      uri,
      headers: {
        HttpHeaders.authorizationHeader:
            'Token 74cdf0ede139bc760b3eac6899cb68af57111e2c',
        HttpHeaders.contentTypeHeader: 'application/json'
      },
    );

    if (response.statusCode == 200) {
      list = json.decode(utf8.decode(response.bodyBytes)) as List;
      setState(() {
        isFetching = false;
        dataReady = true;
      });
    } else {
      throw Exception('Failed to load json');
    }
  }

  @override
  Widget build(BuildContext context) {
    return dataReady
        ? _getReviseDisplay(list)
        : SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                formField("Number of streaks", (value) {
                  if (value?.isEmpty ?? true) return null;
                  if (int.tryParse(value) > 10)
                    return "Please enter a number lower than 11";
                  return null;
                }, TextInputType.number),
                formField("Contextual ayats to be shown", (value) {
                  if (value?.isEmpty ?? true) return null;
                  if (int.tryParse(value) > 10)
                    return "Please enter a number lower than 11";
                  return null;
                }, TextInputType.number),
                formField("How many ayats to be tested?", (value) {
                  if (value?.isEmpty ?? true) return null;
                  if (int.tryParse(value) > 10)
                    return "Please enter a number lower than 11";
                  return null;
                }, TextInputType.number),
                nhGoButton("Revise", _fetchData)
              ],
            ),
          );
  }

  Widget _getReviseAyatStringPerData(data, test_index) {
    List<Widget> stringsSpan = new List();



    for (var i = 0; i < data.length; i++) {
      var splitstring = data[i]['ayat_string'].split(" ");
      var sn = data[i]['surah_number'];
      // stringsSpan.add(TextSpan(text: " "));

      for (var j = 0; j < splitstring.length; j++) {
        if (test_index.contains(data[i]['ayat_number'])) {
          var sn = data[i]['surah_number'];
          // var hiddenStatus = hiddenStatuses[sn][data[i]['ayat_number']];
          var thestring = splitstring[j];
          thestring = thestring.trim();
          stringsSpan.add(ReviseWord(word: thestring));
        } else {
          var thestring = splitstring[j];
          thestring = thestring.trim();
          stringsSpan.add(QuranWord(thestring));
          stringsSpan.add(Text(" "));
        }
      }

      stringsSpan.add(Text("   "));

      stringsSpan.add(Container(
        // padding: EdgeInsets.symmetric(horizontal: 5),
        child: Center(
            child: Text("${data[i]['ayat_number']}",
                style: TextStyle(
                    fontFamily: 'me',
                    fontSize: 15,
                    fontWeight: FontWeight.bold))),
        width: 30.0,
        height: 30.0,
        decoration: new BoxDecoration(
          color: Colors.lightGreenAccent,
          shape: BoxShape.circle,
        ),
      ));

      stringsSpan.add(Text("   "));

    }

    return new Wrap(
      children: stringsSpan,
      direction: Axis.horizontal,
      textDirection: TextDirection.rtl,
      runSpacing: 3,
      crossAxisAlignment: WrapCrossAlignment.center,
      runAlignment: WrapAlignment.spaceBetween,
    );

  }

  Widget _getCard(chunk) {
    return SingleChildScrollView(
      child: new Builder(
        builder: (BuildContext context) {
          return Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 3.0),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.green),
                  borderRadius: BorderRadius.circular(15)),
              child: _getReviseAyatStringPerData(
                  chunk['data'], chunk['test_index']));
        },
      ),
    );
  }

  Widget _getReviseDisplay(list) {
    if (list.length == 0) return Center(child: CircularProgressIndicator());


    return isFetching ? Center(child: CircularProgressIndicator()) : CarouselSlider(
      height: 400.0,
      items: list.map<Widget>(_getCard).toList(),
    );
  }
}
