import 'package:flutter/material.dart';
import 'models.dart';
import 'requests.dart';
import 'ayat.dart';
import 'common.dart';

class SurahListPage extends StatefulWidget {
  @override
  SurahListState createState() {
    return SurahListState();
  }
}

class ListData {
  List<Hifz> hifzlist;
  List<SurahMeta> surahlist;
}

class SurahListState extends State<SurahListPage> {
  ListData _data = new ListData();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder<List<Hifz>>(
      future: fetchHifz(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Hifz> hifzlist = snapshot.data;
          List<int> uniqueSurahNumber =
              hifzlist.map((hifz) => hifz.surah_number).toList();
          uniqueSurahNumber = uniqueSurahNumber.toSet().toList();
          return Container(
              padding: EdgeInsets.only(top: 10),
              child: ListView.builder(
                itemCount: uniqueSurahNumber.length,
                itemBuilder: (context, index) {
                  return FutureBuilder(
                    future: fetchSurahMeta(uniqueSurahNumber[index]),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AyatListPage(
                                          hifzlist: hifzlist
                                              .where((hifz) =>
                                                  hifz.surah_number ==
                                                  uniqueSurahNumber[index])
                                              .toList())));
                            },
                            child: nhSurahNameLabel(snapshot));
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      }
                      return Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Center(
                            child: SizedBox(
                          child: CircularProgressIndicator(),
                          height: 30,
                          width: 30,
                        )),
                      );
                    },
                  );
                },
              ));
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        return Center(child: CircularProgressIndicator());
      },
    ));
  }

  
}
